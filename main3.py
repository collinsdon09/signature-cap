from fastapi import FastAPI, File, UploadFile, Query
from pymongo import MongoClient
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from bson.objectid import ObjectId
from fastapi.responses import JSONResponse, Response
from pydantic import BaseModel
import datetime



app = FastAPI()
origins = [
    "http://localhost:3002",
    "https://2048-41-80-117-206.ngrok-free.app",
    "https://aleatory-repair.000webhostapp.com"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Connect to MongoDB
client = MongoClient('mongodb://localhost:27017/')
db = client['csd']
collection = db['test_collection']
test_collection = db['test']

class SignatureData(BaseModel):
    name: str
    department: str
    wards: list[str]  # List of strings
    grade: str
    signatureImageData: str

@app.post("/upload/")
async def upload_image(image: UploadFile = File(...)):
    # Read the binary data from the uploaded file
    image_data = await image.read()

    # Save the image data as a BLOB in MongoDB and get its ObjectID
    image_id = collection.insert_one(
        {"name": image.filename, "data": image_data}).inserted_id

    # Return the image's ObjectID in the response
    return JSONResponse(content={"image_id": str(image_id)})


@app.post("/your-fastapi-endpoint")
async def process_image_data(image_data: str = Query(...)):
    print("img:", image_data)
    image_id = test_collection.insert_one(
        {"name": "test name", "data": image_data}).inserted_id


@app.get("/get-images/")
async def get_images():
    # Retrieve all image data from MongoDB
    images = test_collection.find({}, {"_id": 1, "name": 1, "signatureImageData": 1})

    # Convert MongoDB cursor to a list of dictionaries
    image_list = list(images)

    # Convert the ObjectIDs to string format for JSON serialization
    for image in image_list:
        image["_id"] = str(image["_id"])

    return image_list


@app.get("/image/{image_id}/")
async def get_image(image_id: str):
    # Retrieve the image data by ObjectID from MongoDB
    image_data = test_collection.find_one(
        {"_id": ObjectId(image_id)}, {"data": 1})

    if not image_data:
        return JSONResponse(content={"error": "Image not found"}, status_code=404)

    # Create a response with the image data
    response = Response(content=image_data["data"])
    # Replace "image/jpeg" with the appropriate content type for your images
    response.headers["Content-Type"] = "image/jpeg"

    return response



@app.post("/your-fastapi-endpoint2")
async def save_signature_data(
    data: SignatureData
):
    # Convert the wards list back to Python list (it comes as a comma-separated string)
    print(data.name,":",  datetime.datetime.now()
)
    # data.wards = data.wards.split(",")

    # Insert the data into the MongoDB collection
    result = test_collection.insert_one(data.dict())

    # Return the inserted document's ID as a response (for demonstration purposes)
    return {"inserted_id": str(result.inserted_id)}





@app.get("/get-nurse/{name}")
def get_member(name: str):
    # Use a case-insensitive regular expression for the name search
    query = {"name": {"$regex": f"^{name}$", "$options": "i"}}

    # Find the document with the specified name using the case-insensitive query
    document = test_collection.find_one(query)

    if document:
        document["_id"] = str(document["_id"])  # Convert ObjectId to string
        return JSONResponse(content=document)
    else:
        return JSONResponse(content={"message": "Document not found"})


@app.get("/get-nurse/{nurse_id}")
def get_nurse_by_id(nurse_id: str):
    try:
        # Convert nurse_id to ObjectId
        nurse_obj_id = ObjectId(nurse_id)
        
        # Find the document with the specified nurse_id
        document = test_collection.find_one({"_id": nurse_obj_id})

        if document:
            document["_id"] = str(document["_id"])  # Convert ObjectId to string
            return JSONResponse(content=document)
        else:
            raise HTTPException(status_code=404, detail="Nurse document not found")
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    


    




@app.put("/edit-nurse/{nurse_id}")
def edit_nurse(nurse_id: str, updated_nurse: dict):
    # Convert nurse_id to ObjectId
    nurse_obj_id = ObjectId(nurse_id)
    print("updated dict", updated_nurse)

    # Update the nurse document
    result = test_collection.update_one({"_id": nurse_obj_id}, {"$set": updated_nurse})

    if result.matched_count == 1:
        return JSONResponse(content={"message": "Nurse document updated successfully"})
    else:
        raise HTTPException(status_code=404, detail="Nurse document not found")
    

@app.delete("/delete-nurse/{item_id}")
async def delete_item(item_id: str):
    # Convert the string ID to an ObjectId
    obj_id = ObjectId(item_id)
    
    # Find the document by ID and delete it
    result = test_collection.delete_one({"_id": obj_id})
    
    if result.deleted_count == 1:
        return {"message": "Item deleted successfully"}
    else:
        raise HTTPException(status_code=404, detail="Item not found")